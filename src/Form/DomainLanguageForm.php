<?php

namespace Drupal\domain_language\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DomainLanguageForm.
 *
 * @package Drupal\domain_language\Form
 */
class DomainLanguageForm extends FormBase {

  use StringTranslationTrait;

  const DEFAULT_LANGUAGE_SITE = '***LANGUAGE_site_default***';

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The system site configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $systemSiteConfig;

  /**
   * The domain site configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $domainSiteConfig;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a new DomainLanguageForm object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The router builder service.
   */
  public function __construct(RouteMatchInterface $route_match, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder) {
    $this->routeMatch = $route_match;
    $this->languageManager = $language_manager;
    $this->systemSiteConfig = $config_factory->get('system.site');
    $this->configFactory = $config_factory;
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_language_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var DomainInterface $domain */
    $domain = $this->routeMatch->getParameter('domain');

    // All available languages.
    $languages = $this->languageManager->getNativeLanguages();

    // Config without any override.
    $configRaw = $this->systemSiteConfig->getRawData();
    $defaultRaw = $configRaw['default_langcode'];

    // Load the domain default language.
    $config = $this->configFactory->get('domain.config.' . $domain->getOriginalId() . '.system.site')->getRawData();
    $defaultLanguage = isset($config['default_langcode']) ? $config['default_langcode'] : self::DEFAULT_LANGUAGE_SITE;

    /** @var LanguageInterface $defaultLanguageRaw */
    $defaultLanguageRaw = $this->languageManager->getLanguage($defaultRaw);

    $options = [
      self::DEFAULT_LANGUAGE_SITE => $this->t(
        "Site's default language (@lang_name)",
        ['@lang_name' => $defaultLanguageRaw->getName()]
      ),
    ];
    foreach ($languages as $language) {
      $options[$language->getId()] = $language->getName();
    }

    $form['domain_id'] = [
      '#type' => 'value',
      '#value' => $domain->getOriginalId(),
    ];

    $form['default_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Default language'),
      '#options' => $options,
      '#description' => $this->t(
        'This will override the default language: %default.',
        ['%default' => $defaultLanguageRaw->getName()]
      ),
      '#default_value' => $defaultLanguage,
      '#required' => TRUE,
    ];

    $options = [];
    foreach ($languages as $language) {
      $options[$language->getId()] = $language->getName();
    }

    $config = $this->configFactory->get('domain.language.' . $domain->getOriginalId() . '.language.negotiation');
    $data = $config->getRawData();

    $form['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages allowed'),
      '#description' => $this->t('If none selected, all will be available. Default language will be added automatically.'),
      '#options' => $options,
      '#default_value' => isset($data['languages']) ? $data['languages'] : [],
      '#required' => FALSE,
    ];

    $form['actions'] = [
      '#type' => 'container',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    $form['actions']['cancel'] = Link::createFromRoute($this->t('Cancel'), 'domain.admin')->toRenderable();
    $form['actions']['cancel']['#attributes']['class'] = ['button', 'button--danger'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domain_id = $form_state->getValue('domain_id');
    $default_language = $form_state->getValue('default_language');
    $languages = array_filter($form_state->getValue('languages'));

    // Set default language into domain config file.
    $config = $this->configFactory->getEditable('domain.config.' . $domain_id . '.system.site');
    if ($default_language == self::DEFAULT_LANGUAGE_SITE) {
      $data = $config->getRawData();
      unset($data['default_langcode']);
      $config->setData($data);

      if (empty($data)) {
        // Delete if config is now empty.
        $config->delete();
      }
      else {
        $config->save();
      }
    }
    else {
      $config->set('default_langcode', $default_language);
      $config->save();
    }

    // Set default language into domain language file.
    $config = $this->configFactory->getEditable('domain.language.' . $domain_id . '.language.negotiation');
    if (empty($languages)) {
      $config->delete();
    }
    else {
      $languages[$default_language] = $default_language;
      $config->set('languages', $languages);
      $config->save();
    }

    // Remove any prefixes and domains in negotiation settings from domain.config file to avoid
    // any unexpected override.
    $config = $this->configFactory->getEditable('domain.config.' . $domain_id . '.language.negotiation');
    $data = $config->getRawData();
    unset($data['url']['prefixes'], $data['url']['domains']);
    if (empty($data['url'])) {
      unset($data['url']);
    }
    if (empty($data)) {
      $config->delete();
    }
    else {
      $config->setData($data);
      $config->save();
    }

    $this->routeBuilder->rebuild();

    // Redirect to domain admin page.
    $form_state->setRedirect('domain.admin');
    $this->messenger()->addStatus('Domain language settings have been updated.');
  }

}
