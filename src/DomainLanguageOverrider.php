<?php

namespace Drupal\domain_language;

use Drupal\domain\DomainNegotiatorInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\domain_config\OverriderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides configuration values based on the active domain and language.
 *
 * @package Drupal\domain_language
 */
class DomainLanguageOverrider implements ConfigFactoryOverrideInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The domain config overrider service.
   *
   * @var \Drupal\domain_config\OverriderInterface
   */
  protected $overrider;

  /**
   * The domain context of the request.
   *
   * @var \Drupal\domain\DomainInterface
   */
  protected $domain;

  /**
   * Nested Level.
   *
   * @var int
   */
  protected $nestedLevel;

  /**
   * Indicates that the request context is set.
   *
   * @var bool
   */
  protected $contextSet;

  /**
   * Constructs a DomainLanguageOverrider object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\domain_config\OverriderInterface $overrider
   *   The domain config overrider service.
   */
  public function __construct(AccountProxyInterface $current_user, DomainNegotiatorInterface $domain_negotiator, ConfigFactoryInterface $config_factory, OverriderInterface $overrider) {
    $this->currentUser = $current_user;
    $this->domainNegotiator = $domain_negotiator;
    $this->configFactory = $config_factory;
    $this->overrider = $overrider;
    $this->nestedLevel = 0;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('domain.negotiator'),
      $container->get('config.factory'),
      $container->get('domain_config.overrider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    global $config;

    $this->nestedLevel++;
    $overrides = [];
    if (empty($this->contextSet)) {
      $this->initiateContext();
    }

    if ($this->domain) {
      foreach ($names as $name) {
        if ($name == 'system.site' && !$this->currentUser->hasPermission('bypass language restrictions')) {
          $configs = $this->overrider->loadOverrides([$name]);

          if (!empty($configs[$name])) {
            // Initialize site settings.
            $overrides[$name] = $configs[$name];
          }
        }
        elseif ($name == 'language.negotiation' && !$this->currentUser->hasPermission('bypass language restrictions')) {
          $languages = $this->configFactory->getEditable('domain.language.' . $this->domain->getOriginalId() . '.' . $name)->get(
            'languages'
          );

          // Setting array as null will reset keys.
          if (!empty($languages)) {
            // Todo: use alter mechanism to properly remove array entries.
            // @see: https://www.drupal.org/node/2829242
            $overrides[$name]['url']['prefixes'] = NULL;
            $overrides[$name]['url']['domains'] = NULL;

            // Set global config var to use second override mechanisms.
            if ($this->nestedLevel == 1) {
              $negotiations = $this->configFactory->getEditable($name)->getRawData();

              // Todo: use 'Drupal\Core\Site\Settings' when available.
              // @see: https://www.drupal.org/node/2183591
              $config[$name]['url']['prefixes'] = array_intersect_key(
                $negotiations['url']['prefixes'],
                $languages
              );
              $config[$name]['url']['domains'] = array_intersect_key(
                $negotiations['url']['domains'],
                $languages
              );
            }
          }
        }
      }
    }

    $this->nestedLevel--;

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    $suffix = $this->domain ? $this->domain->id() : '';

    return ($suffix) ? $suffix : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = ConfigFactoryInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    if (empty($this->contextSet)) {
      $this->initiateContext();
    }
    $metadata = new CacheableMetadata();
    if (!empty($this->domain)) {
      $metadata->addCacheContexts(['url.site']);
    }

    return $metadata;
  }

  /**
   * Sets domain and language contexts for the request.
   *
   * We wait to do this in order to avoid circular dependencies
   * with the locale module.
   */
  protected function initiateContext() {
    $this->contextSet = TRUE;
    $this->domain = $this->domainNegotiator->getActiveDomain();
    // If we have fired too early in the bootstrap, we must force the routine to run.
    if (empty($this->domain)) {
      $this->domain = $this->domainNegotiator->getActiveDomain(TRUE);
    }
  }

}
